# Firejail profile for newsbear
# This file is overwritten after every install/update
# Persistent local customizations
include newsbear.local
# Persistent global definitions
include globals.local

ignore noexec ${HOME}

blacklist /tmp/.X11-unix
blacklist ${RUNUSER}/wayland-*
blacklist /usr/libexec

# Allow python (blacklisted by disable-interpreters.inc)
include allow-python3.inc

include disable-common.inc
include disable-devel.inc
include disable-exec.inc
include disable-interpreters.inc
include disable-programs.inc
include disable-shell.inc
include disable-xdg.inc

whitelist ${HOME}/.local/bin/newsbear
include whitelist-common.inc
include whitelist-runuser-common.inc
include whitelist-usr-share-common.inc
include whitelist-var-common.inc

caps.drop all
ipc-namespace
machine-id
netfilter
no3d
nodvd
nogroups
noinput
nonewprivs
noroot
nosound
notv
nou2f
novideo
protocol unix,inet,inet6
seccomp
seccomp.block-secondary
shell none
tracelog

disable-mnt
private-bin python3*
private-cache
private-dev
private-etc alternatives,ca-certificates,crypto-policies,hostname,hosts,ld.so.cache,ld.so.conf,ld.so.conf.d,ld.so.preload,pki,resolv.conf,ssl
private-tmp

dbus-user filter
dbus-user.own io.gitlab.rusty_snake.NewsBear
dbus-user.talk org.freedesktop.Notifications
dbus-user.talk org.freedesktop.portal.Desktop
dbus-system none

read-only ${HOME}
