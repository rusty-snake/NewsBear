#!/usr/bin/python3

# Copyright © 2021 rusty-snake
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
NewsBear
========

Ein Skript, das tagesschau.de auf Eilmeldungen überwacht und den
Benutzer mittels nativer Benachrichtigungen benachrichtigt, wenn es
eine neu Eilmeldung gibt.

Requirements
------------

 * python3.9 or higher
 * python3-beautifulsoup4
 * python3-requests
 * python3-gobject
"""

__version__ = "2.1.0"

import html
import logging
import os
import sys

if sys.version_info.minor < 9:
    print("newsbear requires python 3.9 or higher.")
    sys.exit(5)

try:
    import requests
except ModuleNotFoundError as err:
    print("Failed to import requests:", err)
    print("Please make sure `requests' is installed.")
    print("  * apt install python3-requests")
    print("  * dnf install python3-requests")
    print("  * pacman -S python-requests")
    print("  * pip install requests")
    sys.exit(5)

try:
    from bs4 import BeautifulSoup
except ModuleNotFoundError as err:
    print("Failed to import BeautifulSoup:", err)
    print("Please make sure `beautifulsoup4' is installed.")
    print("  * apt install python3-bs4")
    print("  * dnf install python3-beautifulsoup4")
    print("  * pacman -S python-beautifulsoup4")
    print("  * pip install beautifulsoup4")
    sys.exit(5)

try:
    from gi.repository import Gio, GLib
except ImportError as err:
    print("Failed to import gio/glib:", err)
    sys.exit(5)


class NewsBear(Gio.Application):
    """NewsBear App"""

    APP_ID = "io.gitlab.rusty_snake.NewsBear"

    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id=self.APP_ID,
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            **kwargs,
        )

        # HACK: Without this --help is not handled by Gio.Application.do_local_command_line
        self.add_main_option(
            "", ord("\0"), GLib.OptionFlags.HIDDEN, GLib.OptionArg.NONE, "", None
        )

        self.last_sent_report = None
        self.open_uri_portal = None

    def do_startup(self):
        Gio.Application.do_startup(self)

        open_uri = Gio.SimpleAction.new("open-uri", GLib.VariantType.new("s"))
        open_uri.connect("activate", self.open_uri)
        self.add_action(open_uri)

    def do_command_line(self, cmdl):
        Gio.Application.do_command_line(self, cmdl)
        if cmdl.get_is_remote() is False:
            # We are the primary instance.
            # self.hold() to stay alive.
            self.hold()
            # Add a periodic call to self.check to the main loop.
            GLib.timeout_add_seconds(60 * 5, self.check)
            # And call it once without timeout.
            GLib.idle_add(self.check, False)
        return 0

    def check(self, rv=True):
        """Check and notify if necessary"""

        try:
            data = self._get("https://www.tagesschau.de/index.html")
            if report := self._extract_breakingnews(data):
                logging.info("BREAKINGNEWS: %s", report["headline"])
                self._notify(report)
        except (requests.HTTPError, requests.exceptions.ConnectionError) as err:
            logging.error("Download from tagesschau.de failed: %s", err)
        return rv

    def open_uri(self, action, uri):
        if not self.open_uri_portal:
            self.open_uri_portal = Gio.DBusProxy.new_sync(
                self.get_dbus_connection(),
                Gio.DBusProxyFlags.NONE,
                None,
                "org.freedesktop.portal.Desktop",
                "/org/freedesktop/portal/desktop",
                "org.freedesktop.portal.OpenURI",
                None,
            )
        self.open_uri_portal.OpenURI("(ssa{sv})", "", uri.get_string(), [])

    @staticmethod
    def _get(url):
        """HTTP GET ``url`` as string"""

        response = requests.get(url)
        response.raise_for_status()
        return response.text

    @staticmethod
    def _extract_breakingnews(ugly_soup):
        """Returns a dict with the report in `ugly_soup` or `None`"""

        breakingnews = BeautifulSoup(ugly_soup, "html.parser").find(
            attrs={"class": "eilmeldung"}
        )
        if breakingnews:
            if not breakingnews.a["href"].startswith("https://www.tagesschau.de/"):
                raise RuntimeError(
                    "Links starting with something else then"
                    " 'https://www.tagesschau.de/' are forbidden."
                )
            return {
                "headline": html.escape(
                    breakingnews.find(attrs={"class": "eilmeldung__headline"}).text
                ),
                "teasertext": html.escape(
                    breakingnews.find(attrs={"class": "eilmeldung__text"}).text
                ),
                "link": breakingnews.a["href"],
            }
        return None

    def _notify(self, report):
        """Send a notification for `report` to the user"""

        if self.last_sent_report == hash(report["headline"]):
            logging.debug(
                """Skip notification for "%s", it has already been notified.""",
                report["headline"],
            )
            return

        self.last_sent_report = hash(report["headline"])

        notification = Gio.Notification.new(report["headline"])
        notification.set_body(report["teasertext"])
        # TODO: Use the tagesschau favicon here.
        notification.set_icon(Gio.Icon.new_for_string("emblem-important-symbolic"))
        notification.set_priority(Gio.NotificationPriority.HIGH)
        notification.set_default_action_and_target(
            "app.open-uri", GLib.Variant.new_string(report["link"])
        )
        # FIXME
        # This uses org.gtk.Notifications (which does not work) if available and uses
        # org.freedesktop.Notifications (which works) as fallback. As I understand it
        # org.gtk.Notifications requires that the sender is a well-known, activable name.
        # We are only well-known but not activable. Using firejail to block org.gtk.Notifications
        # is the only way to make it wotk at time of writing.
        self.send_notification(None, notification)


if __name__ == "__main__":
    try:
        logging.basicConfig(
            format="%(levelname)s(newsbear): %(message)s",
            level=os.getenv("NEWSBEAR_LOGLEVEL", "WARNING"),
        )
        newsbear = NewsBear()
        sys.exit(newsbear.run(sys.argv))
    except KeyboardInterrupt:
        pass
