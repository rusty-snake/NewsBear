# Moved to <https://codeberg.org/rusty-snake/NewsBear> #

<br>


NewsBear
========

![version: 2.1.0](https://img.shields.io/badge/version-2.1.0-brightgreen)
![license: MIT](https://img.shields.io/badge/license-MIT-green)
[![maintenance-status: passively-maintained](https://img.shields.io/badge/maintenance--status-passively--maintained-yellowgreen)](https://gist.github.com/rusty-snake/574a91f1df9f97ec77ca308d6d731e29)

Ein Skript, das tagesschau.de auf Eilmeldungen überwacht und den
Benutzer mittels nativer Benachrichtigungen benachrichtigt, wenn es
eine neu Eilmeldung gibt.

Installation
------------

Aufgrund eines Bugs ist firejail >= 0.9.64 zwingend erforderlich damit
Benachrichtigungen funktionieren. Folgende Befehle installieren newsbear,
das zugehörige firejail profile und fügen newsbear zum autostart hinzu:

```bash
install -Dm0755 newsbear "$HOME"/.local/bin/newsbear
install -Dm0644 newsbear.desktop "$HOME"/.config/autostart/newsbear.desktop
install -Dm0644 newsbear.profile "$HOME"/.config/firejail/newsbear.profile
```

Sobald du dich das nächste mal Anmeldest wirst du bei neuen Eilmeldungen benachrichtigt.
